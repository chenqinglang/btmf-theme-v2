<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

  <div id="header" class="clearfix">

    <ul class="logo">
      <li><a href="/"><img src="/sites/btmf.org/themes/btmfv2/images/logo.png" alt="北京传统音乐节" /></a></li>
      <?php print render($page['language']); ?>
    </ul>

  </div> <!-- /header -->

  <!-- ______________________ MAIN _______________________ -->

  <div id="main" class="clearfix">

    <div id="content">
      <div id="content-inner" class="inner column center">

        <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
          <div id="content-header">

            <?php print $breadcrumb; ?>

            <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']) ?></div>
            <?php endif; ?>

            <?php if ($title): ?>
              <h1 class="title"><?php //print $title; ?></h1>
            <?php endif; ?>

            <?php print render($title_suffix); ?>
            <?php print $messages; ?>
            <?php print render($page['help']); ?>

            <?php if ($tabs): ?>
              <div class="tabs"><?php print render($tabs); ?></div>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>

      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar first">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-first -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->
      <!-- JiaThis Button BEGIN -->
      <div class="jiathis_style_in">
      <div class="jiathis_style">
      	<span class="jiathis_txt">分享到：</span>
      	<a class="jiathis_button_qzone"></a>
      	<a class="jiathis_button_tsina"></a>
      	<a class="jiathis_button_tqq"></a>
      	<a class="jiathis_button_renren"></a>
      	<a class="jiathis_button_kaixin001"></a>
      	<a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
      	<a class="jiathis_counter_style"></a>
      </div>
      <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1334624431040383" charset="utf-8"></script>
    </div>
    <!-- JiaThis Button END -->
  </div> <!-- /main -->

</div> <!-- /page -->

<div id="footer">
  <div class="copy">版权所有 Copyright, 2012
    电话: 86(0)10-64887115 传真: 86(0)10-64887511  电邮：btmf@btmf.org
    北京朝阳区安翔路一号 中国音乐学院行政楼1002 邮编：100101</div>
</div>

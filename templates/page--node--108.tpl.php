<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <div id="header">
    <ul class="logo">
      <li><a href="/"><img src="/sites/btmf.org/themes/btmfv2/images/logo.png" alt="北京传统音乐节" /></a></li>
      <?php print render($page['language']); ?>
    </ul>
    <ul class="index_titles"><img src="/sites/btmf.org/themes/btmfv2/images/index_01.png" /></ul>
  </div>

  <div class="layout_content">
    <div class="left_box">
      <div class="common">
        <?php print render($page['fpnav']); ?>
      </div>
      <div class="common">
        <div class="announcement">
          <h2>主要活动</h2>
          <p><a href="events/88" >第七届北京公园节</a><br/>
            <a href="events/92" >2012世界高等音乐院校论坛</a><br/>
            <a href="events/98" >第三届校少数民族音乐文化传承学术研讨会</a><br/>
            <a href="events/87" >2012社区音乐教育高峰论坛</a>
          </p>
        </div>
        <div class="schedule">
          <?php print render($page['fpschedule']); ?>
        </div>
      </div>
    </div>
    <div class="right_box">
      <?php print render($page['fpnews']); ?>
      <div class="back">
        <h2><a href="/history/papers">往届回顾</a></h2>
        <a href="/history/papers"><img src="/sites/btmf.org/themes/btmfv2/images/back.jpg" /></a>
      </div>
    </div>
  </div>
  <!-- JiaThis Button BEGIN -->
  <div class="jiathis_style_fp">
    <div class="jiathis_style">
      <span class="jiathis_txt">分享到：</span>
      <a class="jiathis_button_qzone"></a>
      <a class="jiathis_button_tsina"></a>
      <a class="jiathis_button_tqq"></a>
      <a class="jiathis_button_renren"></a>
      <a class="jiathis_button_kaixin001"></a>
      <a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
      <a class="jiathis_counter_style"></a>
    </div>
    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js?uid=1334624431040383" charset="utf-8"></script>
    <!-- JiaThis Button END -->
  </div>
  <div class="org">
    <p>主办单位:北京市教委 文化部民族民间文艺发展中心 承办单位:中国音乐学院
  </div>
</div>


<div id="footer">
  <div class="copy">版权所有 Copyright, 2012  
    电话: 86(0)10-64887115 传真: 86(0)10-64887511  电邮：btmf@btmf.org
    北京朝阳区安翔路一号 中国音乐学院行政楼1002 邮编：100101</div>
  </div>


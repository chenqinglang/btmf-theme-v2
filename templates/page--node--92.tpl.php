<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

  <div id="header" class="clearfix">

    <ul class="logo">
      <li><a href="/"><img src="/sites/btmf.org/themes/btmfv2/images/logo.png" alt="北京传统音乐节" /></a></li>
      <?php print render($page['language']); ?>
    </ul>

  </div> <!-- /header -->

  <!-- ______________________ MAIN _______________________ -->

  <div id="main" class="clearfix">

    <div id="content">
      <div id="content-inner" class="inner column center">

        <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
          <div id="content-header">

            <?php print $breadcrumb; ?>

            <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']) ?></div>
            <?php endif; ?>

            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>

            <?php print render($title_suffix); ?>
            <?php print $messages; ?>
            <?php print render($page['help']); ?>

            <?php if ($tabs): ?>
              <div class="tabs"><?php //print render($tabs); ?></div>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <p>论坛主题"音乐与文化交流:在高等音乐艺术院校之间实现合作"</p>
          <p>主要议题:跨学院研究合作;跨文化合作交流;进行予以实现跨文化交流;使用技术来增强思想、教学和演出的交流;</p>
          <h2><a href="/node/133">论坛议程</a></h2>
          <?php if ($page['content_bottom']): ?>
            <div id="content_bottom"><?php print render($page['content_bottom']) ?></div>
          <?php endif; ?>
        </div>

      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar first">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-first -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->

  </div> <!-- /main -->

</div> <!-- /page -->
<div id="footer">
  <div class="copy">版权所有 Copyright, 2012
    电话: 86(0)10-64887115 传真: 86(0)10-64887511  电邮：btmf@btmf.org
    北京朝阳区安翔路一号 中国音乐学院行政楼1002 邮编：100101</div>
</div>
